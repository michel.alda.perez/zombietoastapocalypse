/**
 * @class Finn Creates a player
 */
class Finn {
  /**
   * Creates a Finn player
   * @param {number} player Player to select assets images - default: 1 - values: [1 - 2]
   * @param {number} x X position to star to draw - default: 0
   * @param {string} looking Position to draw player - default: "right" values: ["right" - "left"]
   */
  constructor(player = 1, x = 0, looking = "right") {
    this.width = 64;
    this.height = 64;
    this.x = x;
    this.y = 0;
    this.vx = 0;
    this.vy = 0;
    this.spriteAnimation = 0;
    this.spritePosition = 0;
    this.playerAction = "stand";
    this.looking = looking;
    this.isJumping = false;
    this.isAttacking = false;
    this.jumpStrenght = 10;
    this.hp = 3;
    this.maxSpeed = 5;
    this.stamina = 1;
    if (player !== 2) {
      this.img = new Image();
      this.img.src = "images/FinnSprite.png";
      this.img.onload = () => {
        this.draw();
      };
      this.imgInverted = new Image();
      this.imgInverted.src = "images/FinnSpriteInverted.png";
      this.imgInverted.onload = () => {
        this.draw();
      };
    } else {
      this.img = new Image();
      this.img.src = "images/FinnSprite2.png";
      this.img.onload = () => {
        this.draw();
      };
      this.imgInverted = new Image();
      this.imgInverted.src = "images/FinnSpriteInverted2.png";
      this.imgInverted.onload = () => {
        this.draw();
      };
    }
  }

  draw() {
    if (this.y > canvas.height - this.height) {
      this.y = canvas.height - this.height;
    } else {
      this.vy++;
    }

    if (this.looking === "right") {
      context.drawImage(
        this.img,
        this.spriteAnimation * 32,
        this.spritePosition,
        32,
        32,
        this.x,
        this.y,
        this.width,
        this.height
      );
      // context.strokeRect(
      //   this.x + 10,
      //   this.y + 10,
      //   this.width - 30,
      //   this.height - 18
      // );
    } else {
      context.drawImage(
        this.imgInverted,
        this.spriteAnimation * 32,
        this.spritePosition,
        32,
        32,
        this.x,
        this.y,
        this.width,
        this.height
      );
      // context.strokeRect(
      //   this.x + 20,
      //   this.y + 10,
      //   this.width - 30,
      //   this.height - 18
      // );
    }
  }

  /**
   * Function to detect collitions with an obstacle
   * @param {Toast} obstacle Refers to a Toast obstacle
   */
  // context.strokeRect(
    //   this.x + 15,
    //   this.y + 32,
    //   this.width - 35,
    //   this.height - 32
    // );
  isTouching(obstacle) {
    if (this.looking === "right") {
      return (
        this.x + 10 < obstacle.x + 15 + obstacle.width - 30 &&
        this.x + 10 + this.width - 30 > obstacle.x + 15 &&
        this.y + 30 < obstacle.y + obstacle.height &&
        this.y + this.height - 30 > obstacle.y
      );
    } else {
      return (
        this.x + 20 < obstacle.x + 10 + obstacle.width - 25 &&
        this.x + 20 + this.width - 30 > obstacle.x + 10 &&
        this.y + 30 < obstacle.y + obstacle.height &&
        this.y + this.height - 30 > obstacle.y
      );
    }
  }

  moveLeft() {
    this.vx -= 5;
  }

  moveRight() {
    this.vx += 5;
  }

  jump() {
    this.vy = -2 * this.jumpStrenght;
  }

  attack() {
    this.isAttacking = true;
  }

  isOnPlataform() {
    if (this.y > 10 * 32) {
      this.y = 10 * 32 + 10;
    }
  }
}

/**
 * @class Toast Creates a toas object
 * @author Michel Aldair Perez Reyes
 */
class Toast {
  /**
   * Constructor of Toast
   * @param {number} x X position to star to draw - default: 0
   * @param {string} looking Looking direction of toast - default: 0 - values ["left" - "right"]
   */
  constructor(x = 0, looking = "left") {
    this.width = 64;
    this.height = 64;
    this.x = x;
    this.y = 0;
    this.vx = 0;
    this.vy = 0;
    this.spriteAnimation = 0;
    this.spritePosition = 0;
    this.looking = looking;
    this.maxSpeed = 3;
    this.hp = 1;
    this.img = new Image();
    this.img.src = "images/ZombieToast.png";
    this.img.onload = () => {
      this.draw();
    };
    this.imgInverted = new Image();
    this.imgInverted.src = "images/ZombieToastInverted.png";
    this.imgInverted.onload = () => {
      this.draw();
    };
  }

  draw() {
    if (this.y > canvas.height - this.height * 2) {
      this.y = canvas.height - this.height * 2;
    } else {
      this.vy += 0.01;
    }

    if (this.looking === "left") {
      context.drawImage(
        this.img,
        this.spriteAnimation * 64,
        this.spritePosition,
        64,
        64,
        this.x,
        this.y,
        this.width,
        this.height
      );
    } else {
      context.drawImage(
        this.imgInverted,
        this.spriteAnimation * 64,
        this.spritePosition,
        64,
        64,
        this.x,
        this.y,
        this.width,
        this.height
      );
    }
    // context.strokeRect(
    //   this.x + 15,
    //   this.y + 32,
    //   this.width - 35,
    //   this.height - 32
    // );
  }
}
