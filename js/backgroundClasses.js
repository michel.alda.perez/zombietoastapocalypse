setObject = {
  customGrassStart: [0, 0, 16, 48],
  customGrassMiddle: [16, 0, 16, 48],
  customGrassEnd: [32, 0, 16, 48],
  water: [80, 80, 16, 16],
  plataform: [96, 80, 16, 16],
};

/**
 * @class BackgroundBase Class to draw the background image with infinity scrolling
 * @author Michel Aldair Perez Reyes
 */

class BackgroundBase {
  constructor() {
    this.x = 0;
    this.y = -10;
    this.width = canvas.width;
    this.height = canvas.height;
    this.img = new Image();
    this.img.src = "images/Background.png";
    this.img.onload = () => {
      this.draw();
    };
  }

  draw() {
    this.x--;
    if (this.x < -canvas.width) this.x = 0;
    context.drawImage(this.img, this.x, this.y, this.width, this.height);
    context.drawImage(
      this.img,
      this.x + this.width,
      this.y,
      this.width,
      this.height
    );
  }
}

/**
 * @class Grass Class to draw grass in the background
 * @author Michel Aldair Perez Reyes
 */
class Grass {
  /**
   * @param {string} type Type of grass to draw - default: "grassCustom"
   * @param {number} x X position to start to draw - default: 0
   * @param {number} y Y position to start to draw - default: 0
   * @param {number} cw Custom Width to draw - default: 0
   */
  constructor(type = "grassCustom", x = 0, y = 0, cw = 0) {
    this.type = type;
    this.x = x * 32;
    this.y = y * 32;
    this.width = 32;
    this.height = 32;
    this.cw = cw;
    this.imageSet = new Image();
    this.imageSet.src = "images/set.png";
    this.imageSet.onload = () => {
      this.draw();
    };
    if (type !== "grassUnique") this.width = 96;
    if (type === "grassCustom") this.height = 96;
  }

  draw() {
    if (this.type === "grassCustom") {
      let aux = this.x;
      //Draws the start from a custom grass width
      context.drawImage(
        this.imageSet,
        setObject.customGrassStart[0],
        setObject.customGrassStart[1],
        setObject.customGrassStart[2],
        setObject.customGrassStart[3],
        this.x,
        this.y,
        32,
        96
      );
      //Draws the custom width of grass
      this.x += 32;
      for (let index = 2; index < this.cw; index++) {
        context.drawImage(
          this.imageSet,
          setObject.customGrassMiddle[0],
          setObject.customGrassMiddle[1],
          setObject.customGrassMiddle[2],
          setObject.customGrassMiddle[3],
          this.x,
          this.y,
          32,
          96
        );
        this.x += 32;
      }
      // Draws the last part of the grass
      context.drawImage(
        this.imageSet,
        setObject.customGrassEnd[0],
        setObject.customGrassEnd[1],
        setObject.customGrassEnd[2],
        setObject.customGrassEnd[3],
        this.x,
        this.y,
        32,
        96
      );
      //Repaint in the beggining of the custom grass
      this.x = aux;
    }
  }
}

/**
 * @class Water Create a water object
 * @author Michel Aldair Perez Reyes
 */
class Water {
  /**
   * Draws a water object in the background
   * @param {number} x X position to start to draw
   * @param {number} y Y position to start to draw
   * @param {number} number Number of width to draw
   */
  constructor(x, y, number) {
    this.x = x * 32;
    this.y = y * 32;
    this.number = number;
    this.width = 32;
    this.height = 32;
    this.img = new Image();
    this.img.src = "images/set.png";
    this.img.onload = () => {
      this.draw();
    };
  }
  draw() {
    let aux = this.x;
    for (let index = 0; index < this.number; index++) {
      context.drawImage(
        this.img,
        setObject.water[0],
        setObject.water[1],
        setObject.water[2],
        setObject.water[3],
        this.x,
        this.y,
        this.width,
        this.height
      );
      this.x += 32;
    }
    this.x = aux;
  }
}

/**
 * @class Plataform Creates a plataform object
 * @author Michel Aldair Perez Reyes
 */
class Plataform {
  /**
   * Draws a plataform in the background
   * @param {*} x X position to start to draw
   * @param {*} y Y position to start to draw
   * @param {*} number Number of width to draw
   */
  constructor(x, y, number) {
    this.x = x * 32;
    this.y = y * 32;
    this.width = 32;
    this.height = 32;
    this.img = new Image();
    this.img.src = "images/set.png";
    this.img.onload = () => {
      this.draw();
    };
    this.number = number;
  }
  draw() {
    let aux = this.x;
    for (let index = 0; index < this.number; index++) {
      context.drawImage(
        this.img,
        setObject.plataform[0],
        setObject.plataform[1],
        setObject.plataform[2],
        setObject.plataform[3],
        this.x,
        this.y,
        this.width,
        this.height
      );
      this.x += 32;
    }
    this.x = aux;
  }
}

/**
 * @class Heart Creates a heart object
 */
class Heart {
  /**
   * Draws a heart object in the background
   * @param {Finn} player Player to draw hearts
   * @param {number} position X position to draw
   */
  constructor(player, position) {
    this.player = player;
    this.x = position;
    this.y = 5;
    this.width = 32;
    this.height = 32;
    this.img = new Image();
    this.img.src = "images/Heart.png";
    this.img.onload = () => {
      this.draw();
    };
  }
  draw() {
    context.drawImage(this.img, this.x, this.y, this.width, this.height);
  }
}
