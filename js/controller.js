/**
 * Object to storage the keys to play
 */
let keys = {
  87: false,
  65: false,
  68: false,
  70: false,
  73: false,
  74: false,
  76: false,
  72: false,
};

/**
 * Document event to listen keydown
 */
document.onkeydown = (e) => {
  e.preventDefault();
  switch (e.keyCode) {
    case 87:
      keys[87] = true;
      if (!finn.isJumping) {
        finn.isJumping = true;
        jump.play();
        finn.playerAction = "jump";
        finn.looking === "right"
          ? (finn.spriteAnimation = 15)
          : (finn.spriteAnimation = 12);
        if (finn.y >= 330) {
          finn.jump();
        }
      }
      break;
    case 65:
      keys[65] = true;
      finn.looking = "left";
      if (finn.vx > -finn.maxSpeed) {
        finn.spriteAnimation = 20;
        finn.playerAction = "left";
        finn.moveLeft();
      }
      break;
    case 68:
      keys[68] = true;
      finn.looking = "right";
      if (finn.vx < finn.maxSpeed) {
        finn.spriteAnimation = 8;
        finn.playerAction = "right";
        finn.moveRight();
      }
      break;
    case 70:
      keys[70] = true;
      if (!finn.isAttacking) {
        finn.isAttacking = true;
        sword.play();
        finn.playerAction = "attack";
        finn.looking === "right"
          ? (finn.spriteAnimation = 25)
          : (finn.spriteAnimation = 2);
        finn.attack();
      }
      break;
    //Player 2 keys
    case 73:
      keys[73] = true;
      if (!finn2.isJumping) {
        finn2.isJumping = true;
        jump.play();
        finn2.playerAction = "jump";
        finn2.looking === "right"
          ? (finn2.spriteAnimation = 15)
          : (finn2.spriteAnimation = 12);
        if (finn2.y >= 330) {
          finn2.jump();
        }
      }
      break;
    case 74:
      keys[74] = true;
      finn2.looking = "left";
      if (finn2.vx > -finn2.maxSpeed) {
        finn2.spriteAnimation = 20;
        finn2.playerAction = "left";
        finn2.moveLeft();
      }
      break;
    case 76:
      keys[76] = true;
      finn2.looking = "right";
      if (finn2.vx < finn2.maxSpeed) {
        finn2.spriteAnimation = 8;
        finn2.playerAction = "right";
        finn2.moveRight();
      }
      break;
    case 72:
      keys[72] = true;
      if (!finn2.isAttacking) {
        finn2.isAttacking = true;
        sword.play();
        finn2.playerAction = "attack";
        finn2.looking === "right"
          ? (finn2.spriteAnimation = 25)
          : (finn2.spriteAnimation = 2);
        finn2.attack();
      }
      break;
  }
};

/**
 * Document event to listen keyup
 */
document.onkeyup = (e) => {
  e.preventDefault();
  keys[e.keyCode] = false;
  if (keys[87] === false) {
    finn.isJumping = false;
  }
  if (keys[65] === false) {
    finn.vx = 0;
  }
  if (keys[68] === false) {
    finn.vx = 0;
  }
  if (keys[70] === false) {
    finn.isAttacking = false;
  }
  if (
    keys[87] === false ||
    keys[65] === false ||
    keys[68] === false ||
    keys[70] === false
  ) {
    finn.playerAction = "stand";
  }

  finn.looking === "right"
    ? (finn.spriteAnimation = 0)
    : (finn.spriteAnimation = 19);

  if (keys[73] === false) {
    finn2.isJumping = false;
  }
  if (keys[74] === false) {
    finn2.vx = 0;
  }
  if (keys[76] === false) {
    finn2.vx = 0;
  }
  if (keys[72] === false) {
    finn2.isAttacking = false;
  }
  if (
    keys[73] === false ||
    keys[74] === false ||
    keys[76] === false ||
    keys[72] === false
  ) {
    finn2.playerAction = "stand";
  }

  finn2.looking === "right"
    ? (finn2.spriteAnimation = 0)
    : (finn2.spriteAnimation = 19);
};
