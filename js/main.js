/**
 * Document variables to manipulation
 */
const canvas = document.querySelector("#canvas");
const context = canvas.getContext("2d");
const startButton = document.querySelector("#start-button");
const vsButton = document.querySelector("#vs-button");
const controlsButton = document.querySelector("#controls-button");
const backButton = document.querySelector("#controls-back");
const backMenu = document.querySelector(".back-menu");
const menuDisplay = document.querySelector(".start-menu");
const game = document.querySelector(".game");
const controlsMenu = document.querySelector(".controls");

/**
 * Auxiliar variables and general game variables
 */
let interval;
let frames = 0;
let gravity = 4.81;
let score = 0;
let level = "1";
let zombieLooking = ["left", "right"];
let vs = false;

/**
 * Variables of music
 */
let mainSound = new Audio("sounds/Menu.mp3");
let normalSound = new Audio("sounds/NormalLevel.mp3");
let apocalypseAudio = new Audio("sounds/ApocalypseLevel.mp3");
let jump = new Audio("sounds/jump.wav");
let dmg = new Audio("sounds/dmg.wav");
let sword = new Audio("sounds/sword.wav");
let toastDead = new Audio("sounds/toastDead.wav");

/**
 * Arrays to storage objects
 */
const enemies = [];
const heartsPlayer1 = [];
const heartsPlayer2 = [];

/**
 * Object to storage all the background
 */
const backgroundObjects = {
  grassArray: [],
  1: ["grass", "grassCustom", 0, 12, 6],
  2: ["grass", "grassCustom", 10, 12, 10],
  3: ["grass", "grassCustom", 26, 12, 4],
  waterArray: [],
  4: ["water", 6, 13, 4],
  5: ["water", 20, 13, 6],
  plataformArray: [],
  6: ["plataform", 6, 12, 4],
  7: ["plataform", 20, 12, 6],
};

/**
 * Function to create all components for background using Object.values
 */
function createBackground() {
  Object.values(backgroundObjects).forEach((element) => {
    switch (element[0]) {
      case "grass":
        element = new Grass(element[1], element[2], element[3], element[4]);
        backgroundObjects.grassArray.push(element);
        break;
      case "water":
        element = new Water(element[1], element[2], element[3]);
        backgroundObjects.waterArray.push(element);
        break;
      case "plataform":
        element = new Plataform(element[1], element[2], element[3]);
        backgroundObjects.plataformArray.push(element);
        break;
    }
  });
}

/**
 * Function to draw all components for background
 */
function drawBackground() {
  backgroundObjects.grassArray.forEach((grass) => {
    grass.draw();
  });
  backgroundObjects.waterArray.forEach((water) => {
    water.draw();
  });
  backgroundObjects.plataformArray.forEach((plataform) => {
    plataform.draw();
  });
}

/**
 * Instances of classes
 */
let mountainsBackground = new BackgroundBase();
let finn = new Finn();
let finn2 = new Finn(2, 900);

/**
 * Function to create basic instances of background
 */
createBackground();
createHearts();
createHearts2();

/**
 * Function to clear canvas
 */
function clearCanvas() {
  context.clearRect(0, 0, canvas.widht, canvas.height);
}

/**
 * Function to control animation of player 1 and 2
 * @param {Finn} player Player to control animations
 */
function finnAnimation(player) {
  if (frames % 10 === 0) {
    if (player.looking === "right") {
      switch (player.playerAction) {
        case "stand":
          player.spriteAnimation === 8
            ? (player.spriteAnimation = 0)
            : player.spriteAnimation++;
          break;
        case "right":
          player.spriteAnimation === 14
            ? (player.spriteAnimation = 8)
            : player.spriteAnimation++;
          break;
        case "jump":
          player.spriteAnimation === 15;
          player.spriteAnimation === 0;
          break;
        case "attack":
          player.spriteAnimation === 27
            ? (player.spriteAnimation = 23)
            : player.spriteAnimation++;
          break;
        case "dmg":
          if (player.spriteAnimation === 17) {
            player.playerAction = "stand";
            player.spriteAnimation = 0;
          } else {
            player.spriteAnimation++;
          }
          break;
      }
    } else if (player.looking === "left") {
      switch (player.playerAction) {
        case "stand":
          player.spriteAnimation === 19
            ? (player.spriteAnimation = 27)
            : player.spriteAnimation--;
          break;
        case "left":
          player.spriteAnimation === 14
            ? (player.spriteAnimation = 20)
            : player.spriteAnimation--;
          break;
        case "jump":
          player.spriteAnimatfinn;
          finn === 12;
          break;
        case "attack":
          player.spriteAnimation === 0
            ? (player.spriteAnimation = 4)
            : player.spriteAnimation--;
          break;
        case "dmg":
          if (player.spriteAnimation === 10) {
            player.playerAction = "stand";
            player.spriteAnimation = 19;
          } else {
            player.spriteAnimation--;
          }
          break;
      }
    }
  }
}

/**
 * Function to control Toast animations
 */
function toastAnimation() {
  enemies.forEach((toast) => {
    if (frames % 10 === 0) {
      switch (toast.looking) {
        case "left":
          if (toast.hp !== 0) {
            if (toast.vx > -0.5) {
              toast.spriteAnimation = 2;
              toast.vx -= 0.5;
            }
            toast.spriteAnimation === 2
              ? (toast.spriteAnimation = 5)
              : toast.spriteAnimation--;
          }
          break;
        case "right":
          if (toast.vx < 0.5) {
            toast.spriteAnimation = 18;
            toast.vx += 0.5;
          }
          toast.spriteAnimation === 22
            ? (toast.spriteAnimation = 18)
            : toast.spriteAnimation++;
          break;
      }
    }
  });
}

/**
 * Function to check if player is out of canvas
 * @param {Finn} player Player to check if is out borders
 */
function checkBorders(player) {
  if (player.x + 10 < 0) {
    player.x = canvas.width - player.width;
  } else if (player.x + 10 > canvas.width - 32) {
    player.x = 0 - 10;
  }

  enemies.forEach((toast) => {
    if (toast.x < 0) {
      toast.looking = "right";
      toast.x = 0;
    } else if (toast.x > 960 - toast.width + 25) {
      toast.looking = "left";
      toast.x = 960 - toast.width + 25;
    }
  });
}

/**
 * Function to generate zombies based on frames module (difficult)
 */
function generateZombie() {
  if (score > 0 && score < 200) {
    difficult(100);
  } else if (score > 200 && score < 400) {
    difficult(90);
  } else if (score > 400 && score < 600) {
    difficult(80);
  } else if (score > 600 && score < 800) {
    difficult(60);
  } else if (score > 800 && score < 1000) {
    difficult(50);
  } else {
    difficult(25);
  }
}

/**
 * Function to create a Toast with random X and Looking
 * @param {number} difficult Number to represent frecuency to draw Toast
 */
function difficult(difficult) {
  if (frames % difficult === 0) {
    let randomX = Math.floor(Math.random() * 900);
    let randomLooking = Math.round(Math.random());
    let toast = new Toast(randomX, zombieLooking[randomLooking]);
    enemies.push(toast);
  }
}

/**
 * Function to draw Toast in enemies Array
 */
function drawZombies() {
  enemies.forEach((zombie) => zombie.draw());
}

/**
 * Function to check players collitions
 */
function checkCollition() {
  enemies.forEach((toast, i) => {
    if (finn.isTouching(toast)) {
      enemies.splice(i, 1);
      toastDead.play();
      if (!finn.isAttacking) {
        finn.hp--;
        dmg.play();
        finn.looking === "right"
          ? (finn.spriteAnimation = 16)
          : (finn.spriteAnimation = 11);
        finn.playerAction = "dmg";
        heartsPlayer1.pop();
      }
    }
    if (vs === true) {
      if (finn2.isTouching(toast)) {
        enemies.splice(i, 1);
        toastDead.play();
        if (!finn2.isAttacking) {
          finn2.hp--;
          dmg.play();
          finn2.looking === "right"
            ? (finn2.spriteAnimation = 16)
            : (finn2.spriteAnimation = 11);
          finn2.playerAction = "dmg";
          heartsPlayer2.pop();
        }
      }
    }
  });
}

/**
 * Function to draw Game Over or Player who wins
 * @param {string} winner String message - default: "Game Over"
 */
function drawWinner(winner = "Game Over") {
  context.fillStyle = "black";
  context.fillRect(0, 0, canvas.width, canvas.height);
  context.font = "60px PixelFont";
  context.textAlign = "center";
  context.fillStyle = "red";
  context.fillText(`${winner}`, canvas.width / 2, canvas.height / 2);
  context.font = "20px PixelFont";
  context.fillStyle = "white";
  context.fillText(`SCORE ${score}`, canvas.width / 2, canvas.height / 2 + 25);
  context.fillStyle = "white";
  context.fillText(`LEVEL ${level}`, canvas.width / 2, canvas.height / 2 + 50);
  context.closePath();
}

/**
 * Function to stop sounds and clearInterval
 */
function stopAll() {
  mainSound.pause();
  normalSound.pause();
  apocalypseAudio.pause();
  clearInterval(interval);
}

/**
 * Function to check is the game is over it can be if
 * 1 - Enemies in the game are more than 20 entities
 * 2 - If player's helth is 0
 */
function checkGameOver() {
  if (enemies.length > 20) {
    drawWinner("Toasts dominated the world");
    stopAll();
  }
  if (vs === true) {
    if (finn.hp <= 0) {
      drawWinner("Player 2 Wins");
      stopAll();
    } else if (finn2.hp <= 0) {
      drawWinner("Player 1 Wins");
      stopAll();
    }
  } else {
    if (finn.hp <= 0) {
      drawWinner();
      stopAll();
    }
  }
}

/**
 * Function to create hears objects of player 1
 */
function createHearts() {
  let heartPosition = 100;
  for (let index = 0; index < finn.hp; index++) {
    let heart = new Heart(finn, heartPosition);
    heartPosition += 35;
    heartsPlayer1.push(heart);
  }
}

/**
 * Function to create hears objects of player 2
 */
function createHearts2() {
  let heartPosition2 = 850;
  for (let index = 0; index < finn2.hp; index++) {
    let heart = new Heart(finn2, heartPosition2);
    heartPosition2 += 35;
    heartsPlayer2.push(heart);
  }
}

/**
 * Function to draw players hearts in the background
 */
function drawHearts() {
  heartsPlayer1.forEach((heart) => {
    heart.draw();
  });
  context.font = "20px PixelFont";
  context.textAlign = "center";
  context.fillStyle = "black";
  context.fillText("Player 1", 50, 25);
  if (vs === true) {
    heartsPlayer2.forEach((heart) => {
      heart.draw();
    });
    context.font = "20px PixelFont";
    context.textAlign = "center";
    context.fillStyle = "black";
    context.fillText("Player 2", 800, 25);
  }
}

/**
 * Function to draw score in background
 */
function drawScore() {
  if (frames % 10 === 0) {
    score++;
  }
  context.font = "20px PixelFont";
  context.textAlign = "center";
  context.fillStyle = "white";
  context.fillText(`SCORE ${score}`, 480, 25);
}

/**
 * Function to draw level in background
 */
function drawLevel() {
  context.font = "20px PixelFont";
  context.textAlign = "center";
  context.fillStyle = "white";
  if (score > 200) level = "2";
  if (score > 400) level = "3";
  if (score > 600) level = "4";
  if (score > 800) level = "5";
  if (score > 1000) level = "Apocalypse";
  context.fillText(`LEVEL ${level}`, 480, 50);
}

/**
 * Function to draw enemies in background (counter)
 */
function drawEnemies() {
  context.font = "20px PixelFont";
  context.textAlign = "center";
  context.fillStyle = "red";
  context.fillText(`Zombie toasts ${enemies.length}`, 480, 75);
}

/**
 * Function to manipulate audio
 */
function audioControl() {
  if (score > 1) {
    normalSound.play();
    normalSound.loop = true;
  }
  if (score > 1000) {
    normalSound.pause();
    normalSound.loop = false;
    apocalypseAudio.play();
    apocalypseAudio.loop = true;
  }
}

/**
 * Principal function
 */
function update() {
  frames++;
  clearCanvas();
  mountainsBackground.draw();
  drawBackground();
  finn.draw();
  finn.x += finn.vx;
  finn.y += finn.vy;
  finn.y += gravity;
  if (finn.stamina < 1) finn.stamina++;
  finn.isOnPlataform();
  finnAnimation(finn);
  checkBorders(finn);
  if (vs === true) {
    finn2.draw();
    finn2.x += finn2.vx;
    finn2.y += finn2.vy;
    finn2.y += gravity;
    finn2.isOnPlataform();
    finnAnimation(finn2);
    checkBorders(finn2);
  }
  enemies.forEach((toast) => {
    toast.x += toast.vx;
    toast.y += toast.vy;
    toast.y += gravity;
  });
  generateZombie();
  drawZombies();
  toastAnimation();
  checkCollition();
  drawHearts();
  drawScore();
  drawLevel();
  drawEnemies();
  audioControl();
  checkGameOver();
}

/**
 * Display none of containers game, controls, and back menu
 */
game.style.display = "none";
controlsMenu.style.display = "none";
backMenu.style.display = "none";

/**
 * On window load play sound
 */
window.onload = () => {
  mainSound.play();
  mainSound.loop = true;
};

/**
 * startButton event to begin the game in single player
 */
startButton.onclick = (e) => {
  mainSound.pause();
  menuDisplay.style.display = "none";
  game.style.display = "block";
  backMenu.style.display = "block";
  interval = setInterval(update, 1000 / 60);
};

/**
 * vsButton event to begin the game in multiplayer
 */
vsButton.onclick = (e) => {
  mainSound.pause();
  menuDisplay.style.display = "none";
  game.style.display = "block";
  vs = true;
  backMenu.style.display = "block";
  interval = setInterval(update, 1000 / 60);
};

/**
 * constrolsButton event to display constrols menu
 */
controlsButton.onclick = (e) => {
  menuDisplay.style.display = "none";
  controlsMenu.style.display = "block";
};

/**
 * backButton event to return to main menu
 */
backButton.onclick = (e) => {
  menuDisplay.style.display = "block";
  controlsMenu.style.display = "none";
};

/**
 * backMenu to reload the page and the game
 */
backMenu.onclick = (e) => {
  location.reload();
  backMenu.style.display = "none";
  menuDisplay.style.display = "block";
  game.style.display = "none";
};
