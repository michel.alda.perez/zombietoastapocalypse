# ZombieToastApocalypse

_Zombie Toast Apocalypse_ is a survival web game, which is based on defeat all Zombie Toast, can be played in multiplayer to be more funny :). You can play it here [Game](https://michel.alda.perez.gitlab.io/zombietoastapocalypse/)

![MainImage](images/ZombieToastApocalypse.png)

## Getting Started

Use git or the download button (on the right) to download the game source code. Dependencies are only Bootstrap and assets which are included.

Open index.html with your browser, the game should start, if not check errors with your browser debugging utility.

### Prerequisites

_Zombie Toast Apocalypse_ is a JavaScript game so familiarity with JavaScript is required. It is also recommended that you have git and your prefered code editor.

### Graphics/Textures

The game graphics are packed in images/set.png texture (sprite-sheet) and image cutouts (sprites) are defined in characterClasses.js.

### Architecture

The game consists of four main layers: main and controller. Main consists of game logics and objects. Controller consists of keyboards events, BackgroundClasses contains components which compose and renders graphics also as CharacterClassses. Main layer communicate with all the other by reference.

## Versioning

I use [SemVer](http://semver.org/) for versioning. Actual version is 1.0.0-alpha

## Authors

- **Michel Aldair Perez Reyes** - _Initial work_ - [michel.alda.perez](https://gitlab.com/michel.alda.perez)

## License

This project is not licensed.

## Acknowledgments

- I was inspired by personal childhood playing with the classic xbox
- My inspiration was the game Fuzion Frenzy
- KAVAK Project 1
